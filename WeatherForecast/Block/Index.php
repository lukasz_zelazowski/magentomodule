<?php
namespace Lukasz\WeatherForecast\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Lukasz\WeatherForecast\Model\ResourceModel\Forecast\CollectionFactory as ForecastCollectionFactory;

/**
 * Class Index
 * @package Lukasz\WeatherForecast\Block
 */
class Index extends Template
{
    protected $_forecastCollectionFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param ForecastCollectionFactory $forecastCollectionFactory
     */
    public function __construct(Context $context, ForecastCollectionFactory $forecastCollectionFactory)
    {
        $this->_forecastCollectionFactory = $forecastCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Lukasz\WeatherForecast\Model\ResourceModel\Forecast\Collection
     */
    public function getForecastCollection()
    {
        return $this->_forecastCollectionFactory->create();
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getLastForecast()
    {
        $collection = $this->_forecastCollectionFactory->create();
        $lastForecast = $collection->getLastItem();
        return $lastForecast;
    }
}