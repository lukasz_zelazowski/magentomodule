<?php
namespace Lukasz\WeatherForecast\Block\Adminhtml\Button\Forecast;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Delete implements ButtonProviderInterface
{
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Delete constructor.
     * @param Registry $registry
     * @param UrlInterface $url
     */
    public function __construct(Registry $registry, UrlInterface $url)
    {
        $this->registry = $registry;
        $this->url = $url;
    }

    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->getForecastId()) {
            $data = [
                'label' => __('Delete Forecast'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastInterface | null
     */
    private function getForecast()
    {
        return $this->registry->registry('current_forecast');
    }

    /**
     * @return int|null
     */
    private function getForecastId()
    {
        $forecast = $this->getForecast();
        return ($forecast) ? $forecast->getId() : null;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->url->getUrl(
            '*/*/delete',
            [
                'forecast_id' => $this->getforecastId()
            ]
        );
    }
}
