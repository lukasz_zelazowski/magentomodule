<?php
namespace Lukasz\WeatherForecast\WeatherApi;

interface WeatherApiInterface
{
    public function getWeather();
}