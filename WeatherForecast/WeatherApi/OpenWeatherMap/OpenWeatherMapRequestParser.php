<?php
namespace Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap;

use \Zend\Http\Response;

/**
 * Class OpenWeatherMapRequestParser
 * @package Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap
 */
class OpenWeatherMapRequestParser
{
    /**
     * @param Response $response
     * @return array
     * @throws \Exception
     */
    public function parseResponse(Response $response)
    {
        $weather = json_decode($response->getContent(), true);

        if (!$weather)
        {
            throw new \Exception('OpenWeatherMap - Invalid api response!');
        }

        return [
            'city' => $weather['name'] ?? 'brak',
            'main_weather' => $weather['weather'][0]['main'] ?? 'brak',
            'main_description' =>  $weather['weather'][0]['description'] ?? 'brak',
            'temperature' => $weather['main']['temp'] ?? 'brak',
            'pressure' => $weather['main']['pressure'] ?? 'brak',
            'humidity' => $weather['main']['humidity'] ?? 'brak',
            'temp_min' => $weather['main']['temp_min'] ?? 'brak',
            'temp_max' => $weather['main']['temp_max'] ?? 'brak',
            'wind_speed' => $weather['wind']['speed'] ?? 'brak',
            'clouds' => $weather['clouds']['all'] ?? 'brak'
        ];
    }
}