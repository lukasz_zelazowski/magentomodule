<?php
namespace Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap;

use Lukasz\WeatherForecast\Model\ForecastFactory;
use Lukasz\WeatherForecast\WeatherApi\WeatherApiInterface;
use Lukasz\WeatherForecast\Api\ForecastRepositoryInterface;
use \Psr\Log\LoggerInterface;

/**
 * Class OpenWeatherMap
 * @package Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap
 */
class OpenWeatherMap implements WeatherApiInterface
{
    private $forecastFactory;
    private $weatherRequest;
    private $forecastRepository;
    private $logger;

    /**
     * OpenWeatherMap constructor.
     * @param ForecastFactory $forecastFactory
     * @param ForecastRepositoryInterface $forecastRepository
     * @param OpenWeatherMapRequest $weatherRequest
     * @param LoggerInterface $logger
     */
    public function __construct(
        ForecastFactory $forecastFactory,
        ForecastRepositoryInterface $forecastRepository,
        OpenWeatherMapRequest $weatherRequest,
        LoggerInterface $logger
    )
    {
        $this->forecastFactory = $forecastFactory;
        $this->forecastRepository = $forecastRepository;
        $this->weatherRequest = $weatherRequest;
        $this->logger = $logger;
    }

    public function getWeather()
    {
        try {
            $forecast = $this->forecastFactory->create();
            $forecast->setData($this->weatherRequest->getData());
            $this->forecastRepository->save($forecast);
        }
        catch (\Exception $exception)
        {
            $this->logger->error('Cron Weather: ' . $exception->getMessage());
        }
    }
}