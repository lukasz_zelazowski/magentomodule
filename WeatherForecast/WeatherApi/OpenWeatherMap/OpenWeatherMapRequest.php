<?php
namespace Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap;

use Lukasz\WeatherForecast\Constants\ScopeConfig;
use Zend\Http\Client;
use Zend\Http\Request;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;

/**
 * Class OpenWeatherMapRequest
 * @package Lukasz\WeatherForecast\WeatherApi\OpenWeatherMap
 */
class OpenWeatherMapRequest
{
    const API_ROOT_URL = 'https://api.openweathermap.org/data/2.5/weather';

    private $zendClient;
    private $scopeConfig;
    private $requestParser;

    private $apiUrl;

    /**
     * OpenWeatherMapRequest constructor.
     * @param Client $zendClient
     * @param ScopeConfigInterface $scopeConfig
     * @param OpenWeatherMapRequestParser $requestParser
     */
    public function __construct(
        Client $zendClient,
        ScopeConfigInterface $scopeConfig,
        OpenWeatherMapRequestParser $requestParser
    )
    {
        $this->zendClient = $zendClient;
        $this->scopeConfig = $scopeConfig;
        $this->requestParser = $requestParser;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $this->prepareApiRequestParams();

        return $this->sendRequest();
    }

    /**
     * @return array
     */
    private function sendRequest()
    {
        $this->zendClient->reset();
        $this->zendClient->setUri($this->apiUrl);
        $this->zendClient->setMethod(Request::METHOD_GET);
        $this->zendClient->send();
        $response = $this->zendClient->getResponse();

        return $this->requestParser->parseResponse($response);
    }

    /**
     * @throws \Exception
     */
    private function prepareApiRequestParams()
    {
        $city = $this->scopeConfig->getValue(ScopeConfig::PATH_CITY, ScopeInterface::SCOPE_STORE);
        $country = $this->scopeConfig->getValue(ScopeConfig::PATH_COUNTRY, ScopeInterface::SCOPE_STORE);
        $apiKey = $this->scopeConfig->getValue(ScopeConfig::PATH_API_KEY, ScopeInterface::SCOPE_STORE);

        if (!$city)
        {
            throw new \Exception('No city value in scope config db');
        }

        if (!$country)
        {
            throw new \Exception('No country value in scope config db');
        }

        if (!$apiKey)
        {
            throw new \Exception('No api key value in scope config db');
        }

        $this->apiUrl = self::API_ROOT_URL . "?q={$city},{$country}&units=metric&appid={$apiKey}";
    }
}