<?php

namespace Lukasz\WeatherForecast\Model\Forecast;

use Magento\Framework\UrlInterface;
use Lukasz\WeatherForecast\Api\Data\ForecastInterface;

class Url
{
    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(UrlInterface $urlBuilder) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return string
     */
    public function getListUrl()
    {
        return $this->urlBuilder->getUrl('lukasz_weather_forecast/forecast/index');
    }

    /**
     * @param ForecastInterface $forecast
     * @return string
     */
    public function getForecastUrl(ForecastInterface $forecast)
    {
        return $this->urlBuilder->getUrl('lukasz_weather_forecast/forecast/view', ['id' => $forecast->getId()]);
    }
}
