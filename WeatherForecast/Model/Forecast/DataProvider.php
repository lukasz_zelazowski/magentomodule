<?php
namespace Lukasz\WeatherForecast\Model\Forecast;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Lukasz\WeatherForecast\Model\ResourceModel\Forecast\CollectionFactory as ForecastCollectionFactory;

class DataProvider extends AbstractDataProvider
{
    /**
     * Loaded data cache
     *
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param ForecastCollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ForecastCollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Lukasz\WeatherForecast\Model\Forecast $forecast */
        foreach ($items as $forecast) {
            $this->loadedData[$forecast->getId()] = $forecast->getData();
        }
        $data = $this->dataPersistor->get('lukasz_weather_forecast_forecast');
        if (!empty($data)) {
            $forecast = $this->collection->getNewEmptyItem();
            $forecast->setData($data);
            $this->loadedData[$forecast->getId()] = $forecast->getData();
            $this->dataPersistor->clear('lukasz_weather_forecast_forecast');
        }
        return $this->loadedData;
    }
}
