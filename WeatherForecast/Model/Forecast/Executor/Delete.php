<?php
namespace Lukasz\WeatherForecast\Model\Forecast\Executor;

use Lukasz\WeatherForecast\Api\ForecastRepositoryInterface;
use Lukasz\WeatherForecast\Api\ExecutorInterface;

class Delete implements ExecutorInterface
{
    /**
     * @var ForecastRepositoryInterface
     */
    private $forecastRepository;

    /**
     * Delete constructor.
     * @param ForecastRepositoryInterface $forecastRepository
     */
    public function __construct(
        ForecastRepositoryInterface $forecastRepository
    ) {
        $this->forecastRepository = $forecastRepository;
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($id)
    {
        $this->forecastRepository->deleteById($id);
    }
}
