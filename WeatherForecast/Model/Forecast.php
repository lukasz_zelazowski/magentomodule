<?php
namespace Lukasz\WeatherForecast\Model;

use Lukasz\WeatherForecast\Api\Data\ForecastInterface;
use Magento\Framework\Model\AbstractModel;
use Lukasz\WeatherForecast\Model\ResourceModel\Forecast as ForecastResourceModel;

/**
 * @method \Lukasz\WeatherForecast\Model\ResourceModel\Page _getResource()
 * @method \Lukasz\WeatherForecast\Model\ResourceModel\Page getResource()
 */
class Forecast extends AbstractModel implements ForecastInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'lukasz_weatherforecast_forecast';
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'lukasz_weatherforecast_forecast';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'forecast';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ForecastResourceModel::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Page id
     *
     * @return array
     */
    public function getForecastId()
    {
        return $this->getData(ForecastInterface::FORECAST_ID);
    }

    /**
     * set Forecast id
     *
     * @param  int $forecastId
     * @return ForecastInterface
     */
    public function setForecastId($forecastId)
    {
        return $this->setData(ForecastInterface::FORECAST_ID, $forecastId);
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getData(ForecastInterface::CITY);
    }

    /**
     * @param string $mainWeather
     * @return ForecastInterface
     */
    public function setMainWeather($mainWeather)
    {
        return $this->setData(ForecastInterface::MAIN_WEATHER, $mainWeather);
    }

    /**
     * @return string
     */
    public function getMainWeather()
    {
        return $this->getData(ForecastInterface::MAIN_WEATHER);
    }

    /**
     * @return string
     */
    public function getMainDescription()
    {
        return $this->getData(ForecastInterface::MAIN_DESCRIPTION);
    }

    /**
     * @return string
     */
    public function getTemperature()
    {
        return $this->getData(ForecastInterface::TEMPERATURE);
    }

    /**
     * @return string
     */
    public function getPressure()
    {
        return $this->getData(ForecastInterface::PRESSURE);
    }

    /**
     * @return string
     */
    public function getHumidity()
    {
        return $this->getData(ForecastInterface::HUMIDITY);
    }

    /**
     * @return string
     */
    public function getTempMin()
    {
        return $this->getData(ForecastInterface::TEMP_MIN);
    }

    /**
     * @return string
     */
    public function getTempMax()
    {
        return $this->getData(ForecastInterface::TEMP_MAX);
    }

    /**
     * @return string
     */
    public function getWindSpeed()
    {
        return $this->getData(ForecastInterface::WIND_SPEED);
    }

    /**
     * @return string
     */
    public function getClouds()
    {
        return $this->getData(ForecastInterface::CLOUDS);
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(ForecastInterface::CREATED_AT);
    }
}
