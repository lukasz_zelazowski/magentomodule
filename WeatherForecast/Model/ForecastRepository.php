<?php
namespace Lukasz\WeatherForecast\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Lukasz\WeatherForecast\Api\Data\ForecastInterface;
use Lukasz\WeatherForecast\Api\Data\ForecastInterfaceFactory;
use Lukasz\WeatherForecast\Api\Data\ForecastSearchResultInterfaceFactory;
use Lukasz\WeatherForecast\Api\ForecastRepositoryInterface;
use Lukasz\WeatherForecast\Model\ResourceModel\Forecast as ForecastResourceModel;
use Lukasz\WeatherForecast\Model\ResourceModel\Forecast\Collection;
use Lukasz\WeatherForecast\Model\ResourceModel\Forecast\CollectionFactory as ForecastCollectionFactory;

class ForecastRepository implements ForecastRepositoryInterface
{
    /**
     * Cached instances
     *
     * @var array
     */
    protected $instances = [];

    /**
     * Forecast resource model
     *
     * @var ForecastResourceModel
     */
    protected $resource;

    /**
     * Forecast collection factory
     *
     * @var ForecastCollectionFactory
     */
    protected $forecastCollectionFactory;

    /**
     * Forecast interface factory
     *
     * @var ForecastInterfaceFactory
     */
    protected $forecastInterfaceFactory;

    /**
     * Data Object Helper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     *
     * @var ForecastSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * @param ForecastResourceModel $resource
     * @param ForecastCollectionFactory $forecastCollectionFactory
     * @param ForecastnterfaceFactory $forecastInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ForecastSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ForecastResourceModel $resource,
        ForecastCollectionFactory $forecastCollectionFactory,
        ForecastInterfaceFactory $forecastInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        ForecastSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource             = $resource;
        $this->forecastCollectionFactory = $forecastCollectionFactory;
        $this->forecastInterfaceFactory  = $forecastInterfaceFactory;
        $this->dataObjectHelper     = $dataObjectHelper;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save Forecast.
     *
     * @param \Lukasz\WeatherForecast\Api\Data\ForecastInterface $forecast
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ForecastInterface $forecast)
    {
        /** @var ForecastInterface|\Magento\Framework\Model\AbstractModel $forecast */
        try {
            $this->resource->save($forecast);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Forecast: %1',
                $exception->getMessage()
            ));
        }
        return $forecast;
    }

    /**
     * Retrieve Forecast
     *
     * @param int $forecastId
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($forecastId)
    {
        if (!isset($this->instances[$forecastId])) {
            /** @var ForecastInterface|\Magento\Framework\Model\AbstractModel $forecast */
            $forecast = $this->forecastInterfaceFactory->create();
            $this->resource->load($forecast, $forecastId);
            if (!$forecast->getId()) {
                throw new NoSuchEntityException(__('Requested Forecast doesn\'t exist'));
            }
            $this->instances[$forecastId] = $forecast;
        }
        return $this->instances[$forecastId];
    }

    /**
     * Retrieve Forecasts matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Lukasz\WeatherForecast\Api\Data\ForecastSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Lukasz\WeatherForecast\Model\ResourceModel\Forecast\Collection $collection */
        $collection = $this->forecastCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
        } else {
            $collection->addOrder('main_table.' . ForecastInterface::FORECAST_ID, SortOrder::SORT_DESC);
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var ForecastInterface[] $forecasts */
        $forecasts = [];
        /** @var \Lukasz\WeatherForecast\Model\Forecast $forecast */
        foreach ($collection as $forecast) {
            /** @var ForecastInterface $forecastDataObject */
            $forecastDataObject = $this->forecastInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $forecastDataObject,
                $forecast->getData(),
                ForecastInterface::class
            );
            $forecasts[] = $forecastDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($forecasts);
    }

    /**
     * Delete Forecast
     *
     * @param ForecastInterface $forecast
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ForecastInterface $forecast)
    {
        /** @var ForecastInterface|\Magento\Framework\Model\AbstractModel $forecast */
        $id = $forecast->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($forecast);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to removeForecast %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Forecast by ID.
     *
     * @param int $forecastId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($forecastId)
    {
        $forecast = $this->get($forecastId);
        return $this->delete($forecast);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }

    /**
     * clear caches instances
     * @return void
     */
    public function clear()
    {
        $this->instances = [];
    }
}
