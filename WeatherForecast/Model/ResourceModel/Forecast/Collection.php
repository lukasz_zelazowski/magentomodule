<?php
namespace Lukasz\WeatherForecast\Model\ResourceModel\Forecast;

use Lukasz\WeatherForecast\Model\Forecast;
use Lukasz\WeatherForecast\Model\ResourceModel\AbstractCollection;

/**
 * @api
 */
class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Forecast::class,
            \Lukasz\WeatherForecast\Model\ResourceModel\Forecast::class
        );
    }
}
