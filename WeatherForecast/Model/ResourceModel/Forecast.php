<?php
namespace Lukasz\WeatherForecast\Model\ResourceModel;

/**
 * Class Forecast
 * @package Lukasz\WeatherForecast\Model\ResourceModel
 */
class Forecast extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('lukasz_weather_forecast', 'forecast_id');
    }
}
