<?php
namespace Lukasz\WeatherForecast\Constants;

/**
 * Class ScopeConfig
 * @package Lukasz\WeatherForecast\Constants
 */
class ScopeConfig
{
    const PATH_CITY = 'weather_forecast/config_data/city';
    const PATH_COUNTRY = 'weather_forecast/config_data/country';
    const PATH_API_KEY = 'weather_forecast/config_data/api_key';
}