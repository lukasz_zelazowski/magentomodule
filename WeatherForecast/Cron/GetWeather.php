<?php
namespace Lukasz\WeatherForecast\Cron;

use Lukasz\WeatherForecast\WeatherApi\WeatherApiInterface;

/**
 * Class GetWeather
 * @package Lukasz\WeatherForecast\Cron
 */
class GetWeather
{
    protected $weatherApi;

    /**
     * GetWeather constructor.
     * @param WeatherApiInterface $weatherApi
     */
    public function __construct(WeatherApiInterface $weatherApi)
    {
        $this->weatherApi = $weatherApi;
    }

    public function execute()
    {
        $this->weatherApi->getWeather();
    }
}