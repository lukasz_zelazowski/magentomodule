<?php
namespace Lukasz\WeatherForecast\Api;

use Lukasz\WeatherForecast\Api\Data\ForecastInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface ForecastRepositoryInterface
{
    /**
     * @param ForecastInterface $forecast
     * @return ForecastInterface
     */
    public function save(ForecastInterface $forecast);

    /**
     * @param $id
     * @return ForecastInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param ForecastInterface $forecast
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ForecastInterface $forecast);

    /**
     * @param int $forecastId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($forecastId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
