<?php
namespace Lukasz\WeatherForecast\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface ForecastSearchResultInterface
{
    /**
     * get items
     *
     * @return \Lukasz\WeatherForecast\Api\Data\ForecastInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Lukasz\WeatherForecast\Api\Data\ForecastInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
