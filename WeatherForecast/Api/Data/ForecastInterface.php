<?php
namespace Lukasz\WeatherForecast\Api\Data;

/**
 * @api
 */
interface ForecastInterface
{
    const FORECAST_ID = 'forecast_id';
    const CITY = 'city';
    const MAIN_WEATHER = 'main_weather';
    const MAIN_DESCRIPTION = 'main_description';
    const TEMPERATURE = 'temperature';
    const PRESSURE = 'pressure';
    const HUMIDITY = 'humidity';
    const TEMP_MIN = 'temp_min';
    const TEMP_MAX = 'temp_max';
    const WIND_SPEED = 'wind_speed';
    const CLOUDS = 'clouds';
    const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    const IS_ACTIVE = 'is_active';
    /**
     * @var int
     */
    const STATUS_ENABLED = 1;
    /**
     * @var int
     */
    const STATUS_DISABLED = 2;
    /**
     * @param int $id
     * @return ForecastInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return ForecastInterface
     */
    public function setForecastId($id);

    /**
     * @return int
     */
    public function getForecastId();

    /**
     * @return string
     */
    public function getCity();

    /**
     * @return string
     */
    public function getMainWeather();

    /**
     * @return string
     */
    public function getMainDescription();

    /**
     * @return string
     */
    public function getTemperature();

    /**
     * @return string
     */
    public function getPressure();

    /**
     * @return string
     */
    public function getHumidity();

    /**
     * @return string
     */
    public function getTempMin();

    /**
     * @return string
     */
    public function getTempMax();

    /**
     * @return string
     */
    public function getWindSpeed();

    /**
     * @return string
     */
    public function getClouds();

    /**
     * @return string
     */
    public function getCreatedAt();



    /**
     * @param int $isActive
     * @return ForecastInterface
     */
//    public function setIsActive($isActive);

    /**
     * @return int
     */
//    public function getIsActive();
}
