<?php
namespace Lukasz\WeatherForecast\Setup;

use Lukasz\WeatherForecast\Constants\ScopeConfig;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $connection = $setup->getConnection();

        $data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => ScopeConfig::PATH_API_KEY,
            'value' => 'a2c37c198aeb974da70d11bad8796894',
        ];
        $connection->insertOnDuplicate($setup->getTable('core_config_data'), $data, ['value']);

        $data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => ScopeConfig::PATH_CITY,
            'value' => 'Lublin',
        ];
        $connection->insertOnDuplicate($setup->getTable('core_config_data'), $data, ['value']);

        $data = [
            'scope' => 'default',
            'scope_id' => 0,
            'path' => ScopeConfig::PATH_COUNTRY,
            'value' => 'pl',
        ];
        $connection->insertOnDuplicate($setup->getTable('core_config_data'), $data, ['value']);
    }
}