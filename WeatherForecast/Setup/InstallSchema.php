<?php
namespace Lukasz\WeatherForecast\Setup;

use \Magento\Framework\Setup\InstallSchemaInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\DB\Adapter\AdapterInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    private $tableName = 'lukasz_weather_forecast';

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists($this->tableName)) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable($this->tableName)
            )
                ->addColumn(
                    'forecast_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'Forecast ID'
                )
                ->addColumn(
                    'city',
                    Table::TYPE_TEXT,
                    '1024',
                    [],
                    'City'
                )
                ->addColumn(
                    'main_weather',
                    Table::TYPE_TEXT,
                    '1024',
                    [],
                    'Main Weather'
                )
                ->addColumn(
                    'main_description',
                    Table::TYPE_TEXT,
                    '1024',
                    [],
                    'Main Weather Description'
                )
                ->addColumn(
                    'temperature',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Temperature'
                )
                ->addColumn(
                    'pressure',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Pressure'
                )
                ->addColumn(
                    'humidity',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Humidity'
                )
                ->addColumn(
                    'temp_min',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Min Temperature'
                )
                ->addColumn(
                    'temp_max',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Max Temperature'
                )
                ->addColumn(
                    'wind_speed',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Wind Speed'
                )
                ->addColumn(
                    'clouds',
                    Table::TYPE_TEXT,
                    '16',
                    [],
                    'Clouds'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT
                    ],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT_UPDATE
                    ],
                    'Updated At')
                ->setComment('Weather Forecast Table');

            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable($this->tableName),
                $setup->getIdxName(
                    $installer->getTable($this->tableName),
                    ['created_at'],
                    AdapterInterface::INDEX_TYPE_INDEX
                ),
                ['created_at'],
                AdapterInterface::INDEX_TYPE_INDEX
            );
        }

        $installer->endSetup();
    }
}