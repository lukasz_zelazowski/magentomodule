<?php
namespace Lukasz\WeatherForecast\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class ForecastActions
 * @package Lukasz\WeatherForecast\Ui\Component\Listing\Column
 */
class ForecastActions extends Column
{
    /**
     * Url path  to delete
     * @var string
     */
    const URL_PATH_DELETE = 'lukasz_weatherforecast/forecast/delete';

    /**
     * Url builder
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * constructor
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['forecast_id'])) {
                    $item[$this->getData('name')] = [
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'forecast_id' => $item['forecast_id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.city }"'),
                                'message' => __('Are you sure you want to delete the Forecast "${ $.$data.city }" ?')
                            ]
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
}
