<?php
namespace Lukasz\WeatherForecast\Ui\Provider;

interface CollectionProviderInterface
{
    /**
     * @return \Lukasz\WeatherForecast\Model\ResourceModel\AbstractCollection
     */
    public function getCollection();
}
